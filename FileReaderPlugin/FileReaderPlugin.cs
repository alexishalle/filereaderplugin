﻿using System;
using System.IO;

namespace FileReaderPlugin
{
    public class FileFreaderPlugin
    {
        public static readonly string FileReaderPluginName = "FileReader";
        public static readonly string FileReaderPluginVersion = "0.0.1";
        public static readonly string CommandFilePath = @"C:\";
        public static readonly string CommandFileName = "va_command.txt";

        private static readonly object vaProxyLock = new object();

        private static FileSystemWatcher watcher;
        private static dynamic savedVaProxy;


        public static string VA_DisplayName()
        {
            return FileReaderPluginName + " " + FileReaderPluginVersion;
        }

        public static string VA_DisplayInfo()
        {
            return FileReaderPluginName + "\r\nversion " + FileReaderPluginVersion;
        }

        public static Guid VA_Id()
        {
            return new Guid("{5b2deca6-77ac-47eb-b0fc-0cbb07259b11}");
        }

        public static void VA_Init1(dynamic vaProxy)
        {
            savedVaProxy = vaProxy;

            watcher = new FileSystemWatcher(CommandFilePath);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Changed += OnChanged;
            watcher.Filter = CommandFileName;
            watcher.EnableRaisingEvents = true;
        }

        public static void VA_Exit1(dynamic vaProxy)
        {
        }

        public static void VA_StopCommand()
        {
        }

        public static void VA_Invoke1(dynamic vaProxy)
        {
        }

        public static void OnChanged(object sender, FileSystemEventArgs e)
        {
            try
            {
                lock (vaProxyLock)
                {
                    savedVaProxy.WriteToLog("OnChanged");
                    using (StreamReader sr = File.OpenText(e.FullPath))
                    {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            if (savedVaProxy.CommandExists(s))
                            {
                                savedVaProxy.WriteToLog("Executing " + s);
                                savedVaProxy.Command.Execute(s);
                            }
                            else
                            {
                                savedVaProxy.WriteToLog("Cannot execute " + s);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
